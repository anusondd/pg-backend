const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const passport = require('passport');

//config
const config = require('./config/index');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const staffRouter = require('./routes/staff');
const blogRouter = require('./routes/blog');

//middlewares
const passportJWT = require('./middlewares/passportJWT');
const errorHanlder = require('./middlewares/errorHandler');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//connect to mongodb server
mongoose.connect( config.MONGODB_URI ,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
});

//init passport.js
app.use(passport.initialize());

app.use('/api/', indexRouter); //http://localhost:3000/api
app.use('/api/users', usersRouter);//http://localhost:3000/api/users
app.use('/api/staff', staffRouter);//http://localhost:3000/api/staff
app.use('/api/blog', [ passportJWT.isLogin ] ,blogRouter);//http://localhost:3000/api/blog

app.use(errorHanlder);

module.exports = app;
