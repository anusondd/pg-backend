const express = require('express');
const router = express.Router();

const staffController = require('../controllers/staffController')

//http://localhost:3000/api/staff/search?name=j&age=10
router.get('/search', staffController.search );

//http://localhost:3000/api/staff/
router.get('/', staffController.index );

//http://localhost:3000/api/staff/4
router.get('/:id', staffController.show );

//http://localhost:3000/api/staff/
router.post('/', staffController.insert );

//http://localhost:3000/api/staff/4
router.put('/:id', staffController.update );

//http://localhost:3000/api/staff/4
router.delete('/:id', staffController.destroy );


module.exports = router;
