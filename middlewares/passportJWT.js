const passport = require('passport');
const config = require('../config/index');

const User = require('../models/user');

const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = config.JWT_SECRET;
//opts.issuer = 'accounts.examplesoft.com';
//opts.audience = 'yoursite.net';
passport.use(new JwtStrategy(opts, async function (jwt_payload, done) {
    try {
        const user = await User.findById(jwt_payload.id);
        if (!user) {
            return done(new Error('ไม่พบผู้ใช้ในระบบ'), null);
        }
        return done(null, user); // req.user
    } catch (error) {
        done(error)
    }
}));

module.exports.isLogin = (req, res, next) => {

    passport.authenticate('jwt', { session: false }, (err, user, info) => {
        if (err || !user) {
            const error = new Error('กรุณาล็อกอินก่อน');
            error.statusCode = 401;
            throw error;
        }
        req.user = user;
        next();
    })(req, res, next);

}
